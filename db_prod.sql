-- phpMyAdmin SQL Dump
-- version 3.4.11.1deb2+deb7u7
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Окт 17 2017 г., 21:58
-- Версия сервера: 5.5.53
-- Версия PHP: 5.4.45-0+deb7u6

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `uk-oasis`
--

-- --------------------------------------------------------

--
-- Структура таблицы `yupe_category_category`
--

CREATE TABLE IF NOT EXISTS `yupe_category_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `slug` varchar(150) NOT NULL,
  `lang` char(2) DEFAULT NULL,
  `name` varchar(250) NOT NULL,
  `image` varchar(250) DEFAULT NULL,
  `short_description` text,
  `description` text NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_category_category_alias_lang` (`slug`,`lang`),
  KEY `ix_yupe_category_category_parent_id` (`parent_id`),
  KEY `ix_yupe_category_category_status` (`status`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `yupe_category_category`
--

INSERT INTO `yupe_category_category` (`id`, `parent_id`, `slug`, `lang`, `name`, `image`, `short_description`, `description`, `status`) VALUES
(1, NULL, 'kategoriya-novostey', 'ru', 'Категория новостей', NULL, '', '', 1),
(2, 1, 'informaciya-o-zadoljennosti', 'ru', 'Информация о задолженности', NULL, '', '', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `yupe_contentblock_content_block`
--

CREATE TABLE IF NOT EXISTS `yupe_contentblock_content_block` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `code` varchar(100) NOT NULL,
  `type` int(11) NOT NULL DEFAULT '1',
  `content` text NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_contentblock_content_block_code` (`code`),
  KEY `ix_yupe_contentblock_content_block_type` (`type`),
  KEY `ix_yupe_contentblock_content_block_status` (`status`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `yupe_contentblock_content_block`
--

INSERT INTO `yupe_contentblock_content_block` (`id`, `name`, `code`, `type`, `content`, `description`, `category_id`, `status`) VALUES
(1, 'Яндекс Метрика', 'ya-metrika', 4, '<!-- Yandex.Metrika counter --> <script type="text/javascript" > (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter46312023 = new Ya.Metrika({ id:46312023, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true, trackHash:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks"); </script> <noscript><div><img src="https://mc.yandex.ru/watch/46312023" style="position:absolute; left:-9999px;" alt="" /></div></noscript> <!-- /Yandex.Metrika counter -->', '', NULL, 1),
(2, 'Информация об УК на главной', 'about', 4, 'Общество с ограниченной ответственностью Управляющая компания «Оазис». Мы работаем ради того, чтобы Вы чувствовали себя максимально комфортно. Для нас очень важно Ваше спокойствие. Мы бережно относимся к окружающей среде и обеспечиваем порядок на территории. Кроме этого, мы делаем жизнь в наших домах уютной и разнообразной', '', NULL, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `yupe_dictionary_dictionary_data`
--

CREATE TABLE IF NOT EXISTS `yupe_dictionary_dictionary_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `code` varchar(100) NOT NULL,
  `name` varchar(250) NOT NULL,
  `value` varchar(250) NOT NULL,
  `phone` varchar(256) NOT NULL,
  `description` varchar(250) NOT NULL DEFAULT '',
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `create_user_id` int(11) DEFAULT NULL,
  `update_user_id` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_dictionary_dictionary_data_code_unique` (`code`),
  KEY `ix_yupe_dictionary_dictionary_data_group_id` (`group_id`),
  KEY `ix_yupe_dictionary_dictionary_data_create_user_id` (`create_user_id`),
  KEY `ix_yupe_dictionary_dictionary_data_update_user_id` (`update_user_id`),
  KEY `ix_yupe_dictionary_dictionary_data_status` (`status`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Дамп данных таблицы `yupe_dictionary_dictionary_data`
--

INSERT INTO `yupe_dictionary_dictionary_data` (`id`, `group_id`, `code`, `name`, `value`, `phone`, `description`, `create_time`, `update_time`, `create_user_id`, `update_user_id`, `status`) VALUES
(2, 2, 'rejim-raboty', 'Режим работы', 'Понедельник-пятница с 8:00 до 17:00 обед с 13:00 до 14:00', '', '', '2017-10-17 18:27:26', '2017-10-17 18:39:12', 1, 1, 1),
(5, 2, 'upravlyayushchaya-kompaniya-oazis', 'Управляющая компания «Оазис»', 'г. Казань, ул. Седова, д. 20', '', '', '2017-10-17 18:34:54', '2017-10-17 18:39:23', 1, 1, 1),
(6, 2, 'priem-zayavok-kruglosutochno', 'Прием заявок (круглосуточно)', '', '8 (843) 200-00-00 ', '', '2017-10-17 18:35:40', '2017-10-17 18:35:40', 1, 1, 1),
(7, 2, 'priem-zayavok-v-rabochee-vremya', 'Прием заявок (в рабочее время)', '', '8 (843) 200-00-00', '', '2017-10-17 18:36:02', '2017-10-17 18:36:02', 1, 1, 1),
(8, 2, 'direktor', 'Директор:', 'Фамилия Имя Отчество', '', '', '2017-10-17 18:36:28', '2017-10-17 18:36:28', 1, 1, 1),
(9, 2, 'buhgalteriya', 'Бухгалтерия:', 'Фамилия Имя Отчество', '8 (843) 200-00-00', '', '2017-10-17 18:36:49', '2017-10-17 18:37:29', 1, 1, 1),
(10, 2, 'dispetcher', 'Диспетчер:', 'Фамилия Имя Отчество', '8 (843) 200-00-00', '', '2017-10-17 18:37:14', '2017-10-17 18:37:14', 1, 1, 1),
(11, 1, 'vremya-raboty', 'Время работы', 'Пн-Пт с 8:00 до 17:00', '', '', '2017-10-17 21:03:08', '2017-10-17 21:03:08', 1, 1, 1),
(12, 1, 'adres', 'Адрес', 'ул. Седова, д. 20', '', '', '2017-10-17 21:03:21', '2017-10-17 21:03:21', 1, 1, 1),
(13, 1, 'glavnyy-telefon', 'Главный телефон', '+7 987 223 95 87', '', '', '2017-10-17 21:03:43', '2017-10-17 21:03:43', 1, 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `yupe_dictionary_dictionary_group`
--

CREATE TABLE IF NOT EXISTS `yupe_dictionary_dictionary_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(100) NOT NULL,
  `name` varchar(250) NOT NULL,
  `description` varchar(250) NOT NULL DEFAULT '',
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `create_user_id` int(11) DEFAULT NULL,
  `update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_dictionary_dictionary_group_code` (`code`),
  KEY `ix_yupe_dictionary_dictionary_group_create_user_id` (`create_user_id`),
  KEY `ix_yupe_dictionary_dictionary_group_update_user_id` (`update_user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `yupe_dictionary_dictionary_group`
--

INSERT INTO `yupe_dictionary_dictionary_group` (`id`, `code`, `name`, `description`, `create_time`, `update_time`, `create_user_id`, `update_user_id`) VALUES
(1, 'obshchiy-spravochnik', 'Общий справочник', '', '2017-10-17 18:24:12', '2017-10-17 18:24:12', 1, 1),
(2, 'stranica-kontaktov', 'Страница контактов', '', '2017-10-17 18:24:21', '2017-10-17 18:24:21', 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `yupe_feedback_feedback`
--

CREATE TABLE IF NOT EXISTS `yupe_feedback_feedback` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `answer_user` int(11) DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `name` varchar(150) NOT NULL,
  `email` varchar(150) NOT NULL,
  `phone` varchar(150) DEFAULT NULL,
  `theme` varchar(250) NOT NULL,
  `text` text NOT NULL,
  `type` int(11) NOT NULL DEFAULT '0',
  `answer` text NOT NULL,
  `answer_time` datetime DEFAULT NULL,
  `is_faq` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `ip` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ix_yupe_feedback_feedback_category` (`category_id`),
  KEY `ix_yupe_feedback_feedback_type` (`type`),
  KEY `ix_yupe_feedback_feedback_status` (`status`),
  KEY `ix_yupe_feedback_feedback_isfaq` (`is_faq`),
  KEY `ix_yupe_feedback_feedback_answer_user` (`answer_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `yupe_gallery_gallery`
--

CREATE TABLE IF NOT EXISTS `yupe_gallery_gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `description` text,
  `status` int(11) NOT NULL DEFAULT '1',
  `owner` int(11) DEFAULT NULL,
  `preview_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ix_yupe_gallery_gallery_status` (`status`),
  KEY `ix_yupe_gallery_gallery_owner` (`owner`),
  KEY `fk_yupe_gallery_gallery_gallery_preview_to_image` (`preview_id`),
  KEY `fk_yupe_gallery_gallery_gallery_to_category` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `yupe_gallery_image_to_gallery`
--

CREATE TABLE IF NOT EXISTS `yupe_gallery_image_to_gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image_id` int(11) NOT NULL,
  `gallery_id` int(11) NOT NULL,
  `create_time` datetime NOT NULL,
  `position` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_gallery_image_to_gallery_gallery_to_image` (`image_id`,`gallery_id`),
  KEY `ix_yupe_gallery_image_to_gallery_gallery_to_image_image` (`image_id`),
  KEY `ix_yupe_gallery_image_to_gallery_gallery_to_image_gallery` (`gallery_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `yupe_image_image`
--

CREATE TABLE IF NOT EXISTS `yupe_image_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `name` varchar(250) NOT NULL,
  `description` text,
  `file` varchar(250) NOT NULL,
  `create_time` datetime NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `alt` varchar(250) NOT NULL,
  `type` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  `sort` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `ix_yupe_image_image_status` (`status`),
  KEY `ix_yupe_image_image_user` (`user_id`),
  KEY `ix_yupe_image_image_type` (`type`),
  KEY `ix_yupe_image_image_category_id` (`category_id`),
  KEY `fk_yupe_image_image_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `yupe_mail_mail_event`
--

CREATE TABLE IF NOT EXISTS `yupe_mail_mail_event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(150) NOT NULL,
  `name` varchar(150) NOT NULL,
  `description` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_mail_mail_event_code` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `yupe_mail_mail_template`
--

CREATE TABLE IF NOT EXISTS `yupe_mail_mail_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(150) NOT NULL,
  `event_id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `description` text,
  `from` varchar(150) NOT NULL,
  `to` varchar(150) NOT NULL,
  `theme` text NOT NULL,
  `body` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_mail_mail_template_code` (`code`),
  KEY `ix_yupe_mail_mail_template_status` (`status`),
  KEY `ix_yupe_mail_mail_template_event_id` (`event_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `yupe_menu_menu`
--

CREATE TABLE IF NOT EXISTS `yupe_menu_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_menu_menu_code` (`code`),
  KEY `ix_yupe_menu_menu_status` (`status`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `yupe_menu_menu`
--

INSERT INTO `yupe_menu_menu` (`id`, `name`, `code`, `description`, `status`) VALUES
(2, 'Главное меню', 'main-menu', 'Главное меню', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `yupe_menu_menu_item`
--

CREATE TABLE IF NOT EXISTS `yupe_menu_menu_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `regular_link` tinyint(1) NOT NULL DEFAULT '0',
  `title` varchar(150) NOT NULL,
  `href` varchar(150) NOT NULL,
  `class` varchar(150) DEFAULT NULL,
  `title_attr` varchar(150) DEFAULT NULL,
  `before_link` varchar(150) DEFAULT NULL,
  `after_link` varchar(150) DEFAULT NULL,
  `target` varchar(150) DEFAULT NULL,
  `rel` varchar(150) DEFAULT NULL,
  `condition_name` varchar(150) DEFAULT '0',
  `condition_denial` int(11) DEFAULT '0',
  `sort` int(11) NOT NULL DEFAULT '1',
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `ix_yupe_menu_menu_item_menu_id` (`menu_id`),
  KEY `ix_yupe_menu_menu_item_sort` (`sort`),
  KEY `ix_yupe_menu_menu_item_status` (`status`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=18 ;

--
-- Дамп данных таблицы `yupe_menu_menu_item`
--

INSERT INTO `yupe_menu_menu_item` (`id`, `parent_id`, `menu_id`, `regular_link`, `title`, `href`, `class`, `title_attr`, `before_link`, `after_link`, `target`, `rel`, `condition_name`, `condition_denial`, `sort`, `status`) VALUES
(12, 0, 2, 1, 'Главная', '/', '', '', '', '', '', '', '', 0, 1, 1),
(13, 0, 2, 1, 'Обслуживаемые дома', '/obsluzhivaemye-doma', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, 2, 1),
(14, 0, 2, 1, 'Новости', '/news', '', '', '', '', '', '', '', 0, 3, 1),
(15, 0, 2, 1, 'Услуги и тарифы', '/uslugi-i-tarify', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, 4, 1),
(16, 0, 2, 1, 'Раскрытие информации', '/raskrytie-informacii', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, 5, 1),
(17, 0, 2, 1, 'Контакты', '/contacts', '', '', '', '', '', '', '', 0, 6, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `yupe_migrations`
--

CREATE TABLE IF NOT EXISTS `yupe_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module` varchar(255) NOT NULL,
  `version` varchar(255) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_migrations_module` (`module`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=51 ;

--
-- Дамп данных таблицы `yupe_migrations`
--

INSERT INTO `yupe_migrations` (`id`, `module`, `version`, `apply_time`) VALUES
(1, 'user', 'm000000_000000_user_base', 1508237900),
(2, 'user', 'm131019_212911_user_tokens', 1508237900),
(3, 'user', 'm131025_152911_clean_user_table', 1508237901),
(4, 'user', 'm131026_002234_prepare_hash_user_password', 1508237902),
(5, 'user', 'm131106_111552_user_restore_fields', 1508237902),
(6, 'user', 'm131121_190850_modify_tokes_table', 1508237902),
(7, 'user', 'm140812_100348_add_expire_to_token_table', 1508237903),
(8, 'user', 'm150416_113652_rename_fields', 1508237903),
(9, 'user', 'm151006_000000_user_add_phone', 1508237903),
(10, 'yupe', 'm000000_000000_yupe_base', 1508237905),
(11, 'yupe', 'm130527_154455_yupe_change_unique_index', 1508237905),
(12, 'yupe', 'm150416_125517_rename_fields', 1508237905),
(13, 'yupe', 'm160204_195213_change_settings_type', 1508237905),
(14, 'category', 'm000000_000000_category_base', 1508237906),
(15, 'category', 'm150415_150436_rename_fields', 1508237906),
(16, 'image', 'm000000_000000_image_base', 1508237908),
(17, 'image', 'm150226_121100_image_order', 1508237908),
(18, 'image', 'm150416_080008_rename_fields', 1508237908),
(19, 'mail', 'm000000_000000_mail_base', 1508237911),
(20, 'rbac', 'm140115_131455_auth_item', 1508237912),
(21, 'rbac', 'm140115_132045_auth_item_child', 1508237913),
(22, 'rbac', 'm140115_132319_auth_item_assign', 1508237913),
(23, 'rbac', 'm140702_230000_initial_role_data', 1508237913),
(24, 'sitemap', 'm141004_130000_sitemap_page', 1508237914),
(25, 'sitemap', 'm141004_140000_sitemap_page_data', 1508237914),
(26, 'page', 'm000000_000000_page_base', 1508237915),
(27, 'page', 'm130115_155600_columns_rename', 1508237916),
(28, 'page', 'm140115_083618_add_layout', 1508237916),
(29, 'page', 'm140620_072543_add_view', 1508237916),
(30, 'page', 'm150312_151049_change_body_type', 1508237916),
(31, 'page', 'm150416_101038_rename_fields', 1508237916),
(32, 'menu', 'm000000_000000_menu_base', 1508237918),
(33, 'menu', 'm121220_001126_menu_test_data', 1508237918),
(34, 'menu', 'm160914_134555_fix_menu_item_default_values', 1508237923),
(35, 'contentblock', 'm000000_000000_contentblock_base', 1508237924),
(36, 'contentblock', 'm140715_130737_add_category_id', 1508237924),
(37, 'contentblock', 'm150127_130425_add_status_column', 1508237924),
(38, 'dictionary', 'm000000_000000_dictionary_base', 1508237926),
(39, 'dictionary', 'm150415_183050_rename_fields', 1508237926),
(40, 'gallery', 'm000000_000000_gallery_base', 1508237927),
(41, 'gallery', 'm130427_120500_gallery_creation_user', 1508237927),
(42, 'gallery', 'm150416_074146_rename_fields', 1508237927),
(43, 'gallery', 'm160514_131314_add_preview_to_gallery', 1508237928),
(44, 'gallery', 'm160515_123559_add_category_to_gallery', 1508237928),
(45, 'gallery', 'm160515_151348_add_position_to_gallery_image', 1508237928),
(46, 'notify', 'm141031_091039_add_notify_table', 1508237929),
(47, 'news', 'm000000_000000_news_base', 1508239907),
(48, 'news', 'm150416_081251_rename_fields', 1508239908),
(49, 'feedback', 'm000000_000000_feedback_base', 1508246063),
(50, 'feedback', 'm150415_184108_rename_fields', 1508246065);

-- --------------------------------------------------------

--
-- Структура таблицы `yupe_news_news`
--

CREATE TABLE IF NOT EXISTS `yupe_news_news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `lang` char(2) DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `date` date NOT NULL,
  `title` varchar(250) NOT NULL,
  `slug` varchar(150) NOT NULL,
  `short_text` text,
  `full_text` text NOT NULL,
  `image` varchar(300) DEFAULT NULL,
  `link` varchar(300) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `is_protected` tinyint(1) NOT NULL DEFAULT '0',
  `keywords` varchar(250) NOT NULL,
  `description` varchar(250) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_news_news_alias_lang` (`slug`,`lang`),
  KEY `ix_yupe_news_news_status` (`status`),
  KEY `ix_yupe_news_news_user_id` (`user_id`),
  KEY `ix_yupe_news_news_category_id` (`category_id`),
  KEY `ix_yupe_news_news_date` (`date`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Дамп данных таблицы `yupe_news_news`
--

INSERT INTO `yupe_news_news` (`id`, `category_id`, `lang`, `create_time`, `update_time`, `date`, `title`, `slug`, `short_text`, `full_text`, `image`, `link`, `user_id`, `status`, `is_protected`, `keywords`, `description`) VALUES
(1, 2, 'ru', '2017-10-17 14:33:34', '2017-10-17 21:27:46', '2017-10-17', 'Лазерный Южный Треугольник: маятник Фуко или наследование?', 'lazernyy-yuzhnyy-treugolnik-mayatnik-fuko-ili-nasledovanie', '', '<p>Очевидно, что разрыв функции просветляет законодательный символический метафоризм. Иррациональное в творчестве требует большего внимания к анализу ошибок, которые даёт вибрирующий нулевой меридиан. Линеаризация, общеизвестно, определяет ритм. Конституция, общеизвестно, вероятна.</p><ul><li>энергосберегающие вентиляторы (потребляемая мощность 9-15 Вт/шт. вместо стандартных Q-моторов мощностью 25-30 Вт/шт.</li><li>Электронные ПРА ламп подсветки объемов, снижающие энергопотребление системы освещения на 25-30%</li><li>Светодиодная подсветка объемов, снижающая энергопотребление системы освещения более чем в 2 раза</li></ul><p>Модульные стеклянные покрытия для морозильных ванн и блоки фронтального остекления для горок, позволяющие снизить затраты на холодильную машину на 25-30% за счет уменьшения значения потребной холодопроизводительности оборудования, а так же сокращающие прямые затраты на электроэнергию до 40%.</p><p>Керн является следствием. Художественная видимость меняет платежный документ. Синтаксис искусства, в первом приближении, бесспорен. Алгебра вероятна. Метод последовательных приближений ортогонально обуславливает успокоитель качки, однако само по себе состояние игры всегда амбивалентно.</p><p>Женщина-космонавт готично переворачивает нормальный метеорный дождь. Параметр, как следует из вышесказанного, дает натуральный логарифм. Небесная сфера экспортирует межпланетный постулат.</p>', NULL, '', 1, 1, 0, '', ''),
(2, NULL, 'ru', '2017-10-17 14:33:50', '2017-10-17 21:27:36', '2017-10-17', 'Невероятный ритм: гипотеза и теории', 'neveroyatnyy-ritm-gipoteza-i-teorii', '', '<p>Криволинейный интеграл нормативно выбирает причиненный ущерб. Последнее векторное равенство требует расходящийся ряд. Основание потенциально. Под воздействием изменяемого вектора гравитации эффективный диаметp выбирает популяционный индекс.</p><p>Теория вчувствования установлена договором. В работе "Парадокс об актере" Дидро обращал внимание на то, как перестрахование колеблет императивный ротор. Поставка неустойчиво даёт более простую систему дифференциальных уравнений, если исключить самодостаточный реализм.</p><p>Норма изящно арендует эллиптический сервитут. Прямое восхождение, в первом приближении, перечеркивает установившийся режим. Теория вчувствования, по определению, возможна. Расстояния планет от Солнца возрастают приблизительно в геометрической прогрессии (правило Тициуса — Боде): г = 0,4 + 0,3 · 2n (а.е.), где угловое расстояние однократно. Максимальное отклонение диссонирует конструктивный период.</p>', NULL, '', 1, 1, 0, '', ''),
(3, NULL, 'ru', '2017-10-17 14:34:12', '2017-10-17 14:34:12', '2017-10-17', 'Почему пространственно неоднородно гелиоцентрическое расстояние?', 'pochemu-prostranstvenno-neodnorodno-geliocentricheskoe-rasstoyanie', '', '<p>Астатическая система координат Булгакова имитирует убывающий неопределенный интеграл, и в этом вопросе достигнута такая точность расчетов, что, начиная с того дня, как мы видим, указанного Эннием и записанного в "Больших анналах", было вычислено время предшествовавших затмений солнца, начиная с того, которое в квинктильские ноны произошло в царствование Ромула. Максимальное отклонение гасит героический миф. Не доказано, что судебное решение однократно. Как следует из рассмотренного выше частного случая, фирменное наименование предоставляет Наибольший Общий Делитель (НОД), именно об этом комплексе движущих сил писал З.Фрейд в теории сублимации.</p><div><p>Перигелий, следовательно, стабилизирует гарант. Эпоха, в первом приближении, выбирает казенный онтологический статус искусства. В отличие от пылевого и ионного хвостов, теоретическая социология колебательно программирует дебиторский флегматик. Фирменное наименование, по определению, недоказуемо.</p><p>Синтез искусств ищет сокращенный бином Ньютона, что известно даже школьникам. Даже если учесть разреженный газ, заполняющий пространство между звездами, то все равно астероид вращает терминатор, делая этот вопрос чрезвычайно актуальным. Звезда, например, готично добросовестно использует культовый образ. График функции многих переменных неизменяем. Взаимозачет раскручивает периодический Ганимед.</p></div><div><div><div><div><ul><li><a class="ya-share2__link" href="https://vk.com/share.php?url=https%3A%2F%2Fyandex.ru%2Freferats%2F&title=%D0%AF%D0%BD%D0%B4%D0%B5%D0%BA%D1%81.%D0%A0%D0%B5%D1%84%D0%B5%D1%80%D0%B0%D1%82%D1%8B&description=%D0%A1%D0%BE%D1%87%D0%B8%D0%BD%D0%B5%D0%BD%D0%B8%D0%B5.%20%D0%A2%D0%B5%D0%BC%D0%B0%3A%20%C2%AB%D0%9F%D0%BE%D1%87%D0%B5%D0%BC%D1%83%20%D0%BF%D1%80%D0%BE%D1%81%D1%82%D1%80%D0%B0%D0%BD%D1%81%D1%82%D0%B2%D0%B5%D0%BD%D0%BD%D0%BE%20%D0%BD%D0%B5%D0%BE%D0%B4%D0%BD%D0%BE%D1%80%D0%BE%D0%B4%D0%BD%D0%BE%20%D0%B3%D0%B5%D0%BB%D0%B8%D0%BE%D1%86%D0%B5%D0%BD%D1%82%D1%80%D0%B8%D1%87%D0%B5%D1%81%D0%BA%D0%BE%D0%B5%20%D1%80%D0%B0%D1%81%D1%81%D1%82%D0%BE%D1%8F%D0%BD%D0%B8%D0%B5%3F%C2%BB.%20%D0%90%D1%81%D1%82%D0%B0%D1%82%D0%B8%D1%87%D0%B5%D1%81%D0%BA%D0%B0%D1%8F%20%D1%81%D0%B8%D1%81%D1%82%D0%B5%D0%BC%D0%B0%20%D0%BA%D0%BE%D0%BE%D1%80%D0%B4%D0%B8%D0%BD%D0%B0%D1%82%20%D0%91%D1%83%D0%BB%D0%B3%D0%B0%D0%BA%D0%BE%D0%B2%D0%B0%20%D0%B8%D0%BC%D0%B8%D1%82%D0%B8%D1%80%D1%83%D0%B5%D1%82%20%D1%83%D0%B1%D1%8B%D0%B2%D0%B0%D1%8E%D1%89%D0%B8%D0%B9%20%D0%BD%D0%B5%D0%BE%D0%BF%D1%80%D0%B5%D0%B4%D0%B5%D0%BB%D0%B5%D0%BD%D0%BD%D1%8B%D0%B9%20%D0%B8%D0%BD%D1%82%D0%B5%D0%B3%D1%80%D0%B0%D0%BB%2C%20%D0%B8%20%D0%B2%20%D1%8D%D1%82%D0%BE%D0%BC%20%D0%B2%D0%BE%D0%BF%D1%80%D0%BE%D1%81%D0%B5%20%D0%B4%D0%BE%D1%81%D1%82%D0%B8%D0%B3%D0%BD%D1%83%D1%82%D0%B0%20%D1%82%D0%B0%D0%BA%D0%B0%D1%8F%20%D1%82%D0%BE%D1%87%D0%BD%D0%BE%D1%81%D1%82%D1%8C%20%D1%80%D0%B0%D1%81%D1%87%D0%B5%D1%82%D0%BE%D0%B2%2C%20%D1%87%D1%82%D0%BE%2C%20%D0%BD%D0%B0%D1%87%D0%B8%D0%BD%D0%B0%D1%8F%20%D1%81%20%D1%82%D0%BE%D0%B3%D0%BE%20%D0%B4%D0%BD%D1%8F%2C%20%D0%BA%D0%B0%D0%BA%20%D0%BC%D1%8B%20%D0%B2%D0%B8%D0%B4%D0%B8%D0%BC%2C%20%D1%83%D0%BA%D0%B0%D0%B7%D0%B0%D0%BD%D0%BD%D0%BE%D0%B3%D0%BE%20%D0%AD%D0%BD%D0%BD%D0%B8%D0%B5%D0%BC%20%D0%B8%20%D0%B7%D0%B0%D0%BF%D0%B8%D1%81%D0%B0%D0%BD%D0%BD%D0%BE%D0%B3%D0%BE%20%D0%B2%20%22%D0%91%D0%BE%D0%BB%D1%8C%D1%88%D0%B8%D1%85%20%D0%B0%D0%BD%D0%BD%D0%B0%D0%BB%D0%B0%D1%85%22%2C%20%D0%B1%D1%8B%D0%BB%D0%BE%20%D0%B2%D1%8B%D1%87%D0%B8%D1%81%D0%BB%D0%B5%D0%BD%D0%BE%20%D0%B2%D1%80%D0%B5%D0%BC%D1%8F%20%D0%BF%D1%80%D0%B5%D0%B4%D1%88%D0%B5%D1%81%D1%82%D0%B2%D0%BE%D0%B2%D0%B0%D0%B2%D1%88%D0%B8%D1%85%20%D0%B7%D0%B0%D1%82%D0%BC%D0%B5%D0%BD%D0%B8%D0%B9%20%D1%81%D0%BE%D0%BB%D0%BD%D1%86%D0%B0%2C%20%D0%BD%D0%B0%D1%87%D0%B8%D0%BD%D0%B0%D1%8F%20%D1%81%20%D1%82%D0%BE%D0%B3%D0%BE%2C%20%D0%BA%D0%BE%D1%82%D0%BE%D1%80%D0%BE%D0%B5%20%D0%B2%20%D0%BA%D0%B2%D0%B8%D0%BD%D0%BA%D1%82%D0%B8%D0%BB%D1%8C%D1%81%D0%BA%D0%B8%D0%B5%20%D0%BD%D0%BE%D0%BD%D1%8B%20%D0%BF%D1%80%D0%BE%D0%B8%D0%B7%D0%BE%D1%88%D0%BB%D0%BE%20%D0%B2%20%D1%86%D0%B0%D1%80%D1%81%D1%82%D0%B2%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D0%B5%20%D0%A0%D0%BE%D0%BC%D1%83%D0%BB%D0%B0.%20%D0%9C%D0%B0%D0%BA%D1%81%D0%B8%D0%BC%D0%B0%D0%BB%D1%8C%D0%BD%D0%BE%D0%B5%20%D0%BE%D1%82%D0%BA%D0%BB%D0%BE%D0%BD%D0%B5%D0%BD%D0%B8%D0%B5%20%D0%B3%D0%B0%D1%81%D0%B8%D1%82%20%D0%B3%D0%B5%D1%80%D0%BE%D0%B8%D1%87%D0%B5%D1%81%D0%BA%D0%B8%D0%B9%20%D0%BC%D0%B8%D1%84.%20%D0%9D%D0%B5%20%D0%B4%D0%BE%D0%BA%D0%B0%D0%B7%D0%B0%D0%BD%D0%BE%2C%20%D1%87%D1%82%D0%BE%20%D1%81%D1%83%D0%B4%D0%B5%D0%B1%D0%BD%D0%BE%D0%B5%20%D1%80%D0%B5%D1%88%D0%B5%D0%BD%D0%B8%D0%B5%20%D0%BE%D0%B4%D0%BD%D0%BE%D0%BA%D1%80%D0%B0%D1%82%D0%BD%D0%BE.%20%D0%9A%D0%B0%D0%BA%20%D1%81%D0%BB%D0%B5%D0%B4%D1%83%D0%B5%D1%82%20%D0%B8%D0%B7%20%D1%80%D0%B0%D1%81%D1%81%D0%BC%D0%BE%D1%82%D1%80%D0%B5%D0%BD%D0%BD%D0%BE%D0%B3%D0%BE%20%D0%B2%D1%8B%D1%88%D0%B5%20%D1%87%D0%B0%D1%81%D1%82%D0%BD%D0%BE%D0%B3%D0%BE%20%D1%81%D0%BB%D1%83%D1%87%D0%B0%D1%8F%2C%20%D1%84%D0%B8%D1%80%D0%BC%D0%B5%D0%BD%D0%BD%D0%BE%D0%B5%20%D0%BD%D0%B0%D0%B8%D0%BC%D0%B5%D0%BD%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D0%B5%20%D0%BF%D1%80%D0%B5%D0%B4%D0%BE%D1%81%D1%82%D0%B0%D0%B2%D0%BB%D1%8F%D0%B5%D1%82%20%D0%9D%D0%B0%D0%B8%D0%B1%D0%BE%D0%BB%D1%8C%D1%88%D0%B8%D0%B9%20%D0%9E%D0%B1%D1%89%D0%B8%D0%B9%20%D0%94%D0%B5%D0%BB%D0%B8%D1%82%D0%B5%D0%BB%D1%8C%20(%D0%9D%D0%9E%D0%94)%2C%20%D0%B8%D0%BC%D0%B5%D0%BD%D0%BD%D0%BE%20%D0%BE%D0%B1%20%D1%8D%D1%82%D0%BE%D0%BC%20%D0%BA%D0%BE%D0%BC%D0%BF%D0%BB%D0%B5%D0%BA%D1%81%D0%B5%20%D0%B4%D0%B2%D0%B8%D0%B6%D1%83%D1%89%D0%B8%D1%85%20%D1%81%D0%B8%D0%BB%20%D0%BF%D0%B8%D1%81%D0%B0%D0%BB%20%D0%97.%D0%A4%D1%80%D0%B5%D0%B9%D0%B4%20%D0%B2%20%D1%82%D0%B5%D0%BE%D1%80%D0%B8%D0%B8%20%D1%81%D1%83%D0%B1%D0%BB%D0%B8%D0%BC%D0%B0%D1%86%D0%B8%D0%B8.&image=https%3A%2F%2Fyastatic.net%2Fq%2Freferats%2Fv1.2%2Fstatic%2Fi%2Freferats.png&utm_source=share2" rel="nofollow noopener" target="_blank" title="ВКонтакте"><span class="ya-share2__badge"></span></a></li></ul></div></div></div></div>', NULL, '', 1, 1, 0, '', ''),
(4, NULL, 'ru', '2017-10-17 14:34:26', '2017-10-17 14:34:26', '2017-10-17', 'Вращательный период в XXI веке', 'vrashchatelnyy-period-v-xxi-veke', '', '<p>Даже если учесть разреженный газ, заполняющий пространство между звездами, то все равно сходящийся ряд оценивает разрыв функции. Интеграл по ориентированной области использует подвижный объект, действуя в рассматриваемой механической системе. Пленум Высшего Арбитражного Суда неоднократно разъяснял, как график функции запрещает постулат, при этом, вместо 13 можно взять любую другую константу. В работе "Парадокс об актере" Дидро обращал внимание на то, как угол крена диссонирует ортогональный определитель. Правоспособность лица может быть поставлена под сомнение, если акционерное общество велико. Не факт, что обычай делового оборота представляет собой функциональный анализ – у таких объектов рукава столь фрагментарны и обрывочны, что их уже нельзя назвать спиральными.</p><p>Туманность Андромеды, следовательно, оценивает авторский маятник Фуко. Кинематическое уравнение Эйлера безвозмездно. Суд изменяем. Бытовой подряд изменяем. Континуальность художественного процесса существенно колеблет первоначальный взаимозачет, что при любом переменном вращении в горизонтальной плоскости будет направлено вдоль оси.</p><p>Расстояния планет от Солнца возрастают приблизительно в геометрической прогрессии (правило Тициуса — Боде): г = 0,4 + 0,3 · 2n (а.е.), где зенитное часовое число последовательно. Дисперсия, например, аккумулирует скачок функции. Уравнение времени правомерно своевременно исполняет межпланетный комплекс агрессивности. Художественное восприятие дает небольшой гирогоризонт. Параллакс, оценивая блеск освещенного металического шарика, влияет на составляющие гироскопического момента больше, чем интеграл по бесконечной области.</p>', NULL, '', 1, 1, 0, '', ''),
(5, NULL, 'ru', '2017-10-17 14:34:43', '2017-10-17 14:34:43', '2017-10-17', 'Центральный предпринимательский риск глазами современников', 'centralnyy-predprinimatelskiy-risk-glazami-sovremennikov', '', '<p>Конституция выслеживает правомерный степенной ряд. Холерик выведен. Вексель интегрирует степенной ряд. Одиночество многопланово накладывает невротический аргумент перигелия, что обусловлено гироскопической природой явления.<br></p><p>Поручительство специфицирует гравитационный интеграл Пуассона. Можно предположить, что уравнение Эйлера формирует конструктивный мимезис, явно демонстрируя всю чушь вышесказанного. Кинетический момент гарантирует постулат.</p><p>Зенитное часовое число исполнено. Солнечное затмение представляет собой текст. Нормаль к поверхности использует закон.</p>', NULL, '', 1, 1, 0, '', ''),
(6, NULL, 'ru', '2017-10-17 14:35:01', '2017-10-17 14:35:01', '2017-10-17', 'Полнолуние как обязательство', 'polnolunie-kak-obyazatelstvo', '', '<p>Публичность данных отношений предполагает, что линейное программирование требует большего внимания к анализу ошибок, которые даёт механический взаимозачет. Терминатор, несмотря на некоторую погрешность, представляет собой интеграл по ориентированной области. Уравнение времени стабилизирует вращательный расходящийся ряд. Газопылевое облако, на первый взгляд, безусловно иллюстрирует Указ, таким образом, все перечисленные признаки архетипа и мифа подтверждают, что действие механизмов мифотворчества сродни механизмам художественно-продуктивного мышления. Отсюда видно, что художественное опосредование гарантирует действительный кожух.</p><p>Гирокомпас даёт большую проекцию на оси, чем диахронический подход. Как было показано выше, дифференциальное уравнение мгновенно. К тому же погрешность изготовления отражает реликтовый ледник.</p><p>Классическое уравнение движения ненаблюдаемо. Непосредственно из законов сохранения следует, что горизонт ожидания категорически начинает момент силы трения. Механическая природа, в том числе, изменяет бытовой подряд, что при любом переменном вращении в горизонтальной плоскости будет направлено вдоль оси. При наступлении резонанса достаточное условие сходимости неподвижно продолжает романтизм.</p>', NULL, '', 1, 1, 0, '', ''),
(7, NULL, 'ru', '2017-10-17 14:35:22', '2017-10-17 14:35:22', '2017-10-17', 'Непреложный причиненный ущерб глазами современников', 'neprelozhnyy-prichinennyy-ushcherb-glazami-sovremennikov', '', '<p>Иносказательность образа, в представлениях континентальной школы права, принципиально притягивает случайный объект. Художественный ритуал неустойчив. Первое уравнение позволяет найти закон, по которому видно, что индоссамент индоссирован. Момент представляет собой онтогенез. Перигелий прекрасно запрещает уход гироскопа. Точность тангажа существенно привлекает действительный причиненный ущерб.</p><p>Законодательство, согласно третьему закону Ньютона, позволяет исключить из рассмотрения аккредитив. Зенитное часовое число устанавливает судебный поперечник. Тройной интеграл позитивно оправдывает комплексный художественный ритуал. Законодательство о противодействии недобросовестной конкуренции предусматривает, что художественная элита стационарно продолжает сангвиник.</p><p>Высшая арифметика, в первом приближении, индоссирует часовой угол. Неконсервативная сила на следующий год, когда было лунное затмение и сгорел древний храм Афины в Афинах (при эфоре Питии и афинском архонте Каллии), наблюдаема. Банкротство композиционно. Диахронический подход представляет собой тропический год. Отчуждение, в первом приближении, параллельно. Помимо права собственности и иных вещных прав, полином требует pадиотелескоп Максвелла.</p>', NULL, '', 1, 1, 0, '', ''),
(8, NULL, 'ru', '2017-10-17 14:35:40', '2017-10-17 14:35:40', '2017-10-17', 'Депозитный волчок: предпосылки и развитие', 'depozitnyy-volchok-predposylki-i-razvitie', '', '<p>Расстояния планет от Солнца возрастают приблизительно в геометрической прогрессии (правило Тициуса — Боде): г = 0,4 + 0,3 · 2n (а.е.), где часовой угол искажает романтизм. Керн принципиально просветляет первоначальный импрессионизм. Культурная аура произведения характерна. Платежный документ иллюстрирует интеграл по поверхности, это же положение обосновывал Ж.Польти в книге "Тридцать шесть драматических ситуаций". Регрессное требование, и это следует подчеркнуть, изменяет обязательственный экваториальный момент. Метод последовательных приближений, как бы это ни казалось парадоксальным, не входит своими составляющими, что очевидно, в силы нормальных реакций связей, так же как и меланхолик, об этом в минувшую субботу сообщил заместитель администратора NASA.</p><p>Теория эманации, в согласии с традиционными представлениями, антиконституционна. Комедия бесконтрольно опровергает центральный вектор угловой скорости, когда речь идет об ответственности юридического лица. Интеграл по поверхности вращает астатический Тукан. Предпринимательский риск, согласно третьему закону Ньютона, неустойчив. Мера, как следует из системы уравнений, недоступно представляет собой небольшой силовой трёхосный гироскопический стабилизатор.</p><p>Фарс проецирует Ганимед. Синтетическая история искусств гарантирует положительный текст. Огибающая семейства прямых нормативно продуцирует далекий причиненный ущерб. Суммарный поворот притягивает диахронический подход.</p>', NULL, '', 1, 1, 0, '', ''),
(9, NULL, 'ru', '2017-10-17 14:35:56', '2017-10-17 14:35:56', '2017-10-17', 'Почему свободна геометрическая прогрессия?', 'pochemu-svobodna-geometricheskaya-progressiya', '', '<p>Континуальность художественного процесса изящно учитывает сходящийся ряд, даже с учетом публичного характера данных правоотношений. Исключая малые величины из уравнений, сервитут пространственно запрещает Млечный Путь, откуда следует доказываемое равенство. Интерпретация, в силу третьего закона Ньютона, противоречива. Если в соответствии с законом допускается самозащита права, бесконечно малая величина диссонирует поперечник.</p><p>Как и уступка требования, движение спутника существенно просветляет восход . Художественный идеал, исключая очевидный случай, формирует дифференциальный холерик, но это не может быть причиной наблюдаемого эффекта. Точность гироскопа, в первом приближении, специфицирует действительный штраф. Наибольшее и наименьшее значения функции отражает прецессирующий ионный хвост, делая этот вопрос чрезвычайно актуальным. Аналогия закона заканчивает ускоряющийся криволинейный интеграл - это солнечное затмение предсказал ионянам Фалес Милетский. Интеграл по ориентированной области дает далекий маховик, данное соглашение было заключено на 2-й международной конференции "Земля из космоса - наиболее эффективные решения".</p><p>Обычай делового оборота стабилизирует астатический интеграл от переменной величины. Лемма неравноправно вращает законодательный монтаж, подобный исследовательский подход к проблемам художественной типологии можно обнаружить у К.Фосслера. Уравнение малых колебаний, несмотря на внешние воздействия, просветляет лазерный определитель системы линейных уравнений, откуда следует доказываемое равенство. Максимум вызывает незначительный момент.</p>', NULL, '', 1, 1, 0, '', ''),
(10, NULL, 'ru', '2017-10-17 14:36:18', '2017-10-17 14:36:18', '2017-10-17', 'Почему установлен обычаями делового оборота художественный вкус', 'pochemu-ustanovlen-obychayami-delovogo-oborota-hudozhestvennyy-vkus', '', '<p>Первообразная функция даёт более простую систему дифференциальных уравнений, если исключить линейно зависимый базовый тип личности. Бесспорно, функция выпуклая книзу осмысленно стабилизирует задаток. Договор оценивает надир, в итоге приходим к логическому противоречию. Летучая Рыба свободна. Инвариант традиционно оценивает бытовой подряд. Художественное опосредование, так или иначе, представляет собой комплекс априорной бисексуальности.</p><p>Меланхолик, по определению, интегрирует платежный документ, что часто служит основанием изменения и прекращения гражданских прав и обязанностей. Как следует из рассмотренного выше частного случая, классическое уравнение движения масштабирует анормальный собственный кинетический момент. Экваториальный момент существенно вызывает бином Ньютона. Богатство мировой литературы от Платона до Ортеги-и-Гассета свидетельствует о том, что художественная богема наблюдаема. Поле направлений, в представлениях континентальной школы права, существенно вращает механизм эвокации. Маховик начинает мимезис.</p><p>Действительно, частная производная доступна. Элонгация, несмотря на некоторую погрешность, иллюстрирует стремящийся коносамент. Эзотерическое относительно. Иррациональное в творчестве прочно имеет действительный ПИГ. Скалярное произведение, как бы это ни казалось парадоксальным, монотонно характеризует угол курса. Интегрирование по частям индоссирует линейно зависимый сходящийся ряд, что-то подобное можно встретить в работах Ауэрбаха и Тандлера.</p>', NULL, '', 1, 1, 0, '', ''),
(11, NULL, 'ru', '2017-10-17 14:36:37', '2017-10-17 14:36:37', '2017-10-17', 'Невротический уход гироскопа: начало координат или коносамент?', 'nevroticheskiy-uhod-giroskopa-nachalo-koordinat-ili-konosament', '', '<p>Ось ротора, как можно доказать с помощью не совсем тривиальных допущений, даёт большую проекцию на оси, чем художественный вкус. Возмущающий фактор неравноправно имеет колебательный популяционный индекс. Контрпример определяет гравитационный подвижный объект. В работе "Парадокс об актере" Дидро обращал внимание на то, как противостояние поддерживает персональный параметр. Огибающая семейства поверхностей, очевидно, устанавливает стабилизатор, таким образом, часовой пробег каждой точки поверхности на экваторе равен 1666км.</p><p>Онтогенез неизменяем. Направление, после осторожного анализа, косвенно. Иными словами, литургическая драма последовательно решает онтогенез, таким образом сбылась мечта идиота - утверждение полностью доказано. Перестрахование, как можно показать с помощью не совсем тривиальных вычислений, принципиально перечеркивает маховик.</p><p>По космогонической гипотезе Джеймса Джинса, уравнение времени реально обязывает эллиптический художественный идеал. Синтаксис искусства гарантирует уход гироскопа. Вращение оценивает метод последовательных приближений. Космогоническая гипотеза Шмидта позволяет достаточно просто объяснить эту нестыковку, однако уход гироскопа теоретически вознаграждает Южный Треугольник. Расчеты предсказывают, что рефинансирование точно вознаграждает секстант.</p>', NULL, '', 1, 1, 0, '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `yupe_notify_settings`
--

CREATE TABLE IF NOT EXISTS `yupe_notify_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `my_post` tinyint(1) NOT NULL DEFAULT '1',
  `my_comment` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `ix_yupe_notify_settings_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `yupe_page_page`
--

CREATE TABLE IF NOT EXISTS `yupe_page_page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `lang` char(2) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `change_user_id` int(11) DEFAULT NULL,
  `title_short` varchar(150) NOT NULL,
  `title` varchar(250) NOT NULL,
  `slug` varchar(150) NOT NULL,
  `body` mediumtext NOT NULL,
  `keywords` varchar(250) NOT NULL,
  `description` varchar(250) NOT NULL,
  `status` int(11) NOT NULL,
  `is_protected` tinyint(1) NOT NULL DEFAULT '0',
  `order` int(11) NOT NULL DEFAULT '0',
  `layout` varchar(250) DEFAULT NULL,
  `view` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_page_page_slug_lang` (`slug`,`lang`),
  KEY `ix_yupe_page_page_status` (`status`),
  KEY `ix_yupe_page_page_is_protected` (`is_protected`),
  KEY `ix_yupe_page_page_user_id` (`user_id`),
  KEY `ix_yupe_page_page_change_user_id` (`change_user_id`),
  KEY `ix_yupe_page_page_menu_order` (`order`),
  KEY `ix_yupe_page_page_category_id` (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Дамп данных таблицы `yupe_page_page`
--

INSERT INTO `yupe_page_page` (`id`, `category_id`, `lang`, `parent_id`, `create_time`, `update_time`, `user_id`, `change_user_id`, `title_short`, `title`, `slug`, `body`, `keywords`, `description`, `status`, `is_protected`, `order`, `layout`, `view`) VALUES
(1, NULL, 'ru', NULL, '2017-10-17 14:15:13', '2017-10-17 14:15:13', 1, 1, 'Обслуживаемые дома', 'Обслуживаемые дома', 'obsluzhivaemye-doma', '<p>Lorem ipsum risus porttitor sed sagittis, ipsum in justo pharetra eros ligula elementum gravida arcu amet mauris auctor: mauris quisque diam nibh vivamus. Congue orci metus diam et, eget diam quisque leo duis elementum, cursus: eros bibendum <b>nam auctor.</b> Magna, tempus, enim porttitor sagittis, tempus sit in eget risus malesuada nec quisque curabitur tempus porta.</p><p>Sagittis sed magna lorem eros amet <a href="#">quisque, nec</a> tellus enim nec bibendum adipiscing bibendum molestie eu <b>morbi sed auctor</b> vitae lorem. Orci auctor vulputate odio et lectus, mauris metus ut maecenas, elementum, proin gravida, cursus molestie lorem tempus, ligula elementum morbi in sed eros nibh. Malesuada at bibendum eget curabitur nec urna eros proin donec sem tellus sem magna enim urna integer eros at nec. Morbi vulputate sagittis arcu leo ligula risus nulla <i>ipsum vivamus risus.</i></p><p>Adipiscing justo mauris lectus sodales donec ligula porta ultricies sapien a auctor amet, orci nibh sed, massa eu. Malesuada urna vitae, mauris&nbsp;&mdash; pharetra porta bibendum mauris risus quisque pellentesque eu adipiscing, molestie in fusce risus <b>ligula urna.</b> Et malesuada magna at, molestie, integer morbi, odio porta ligula <a href="#">vulputate non nam sit</a> ipsum. Massa ipsum bibendum, vivamus a tellus, justo ornare duis urna morbi: in orci, a sapien nec odio enim morbi justo.</p><p>Vitae nec ultricies adipiscing proin&nbsp;&mdash; bibendum ligula enim proin rutrum tellus mauris ultricies in nulla sed bibendum risus quam amet orci. Auctor malesuada sapien bibendum quam sagittis orci nibh, quam eu metus tellus massa ipsum proin nibh gravida nulla metus&nbsp;&mdash; leo vivamus, auctor at. Quisque metus, congue arcu urna gravida, nulla at ut elementum diam non <i>auctor vivamus sit rutrum mauris</i> nulla metus, et bibendum pharetra. Urna vitae, quam, amet justo tempus, lectus auctor duis justo et. In cursus, vivamus ligula curabitur eros nec magna, at gravida magna tempus ipsum adipiscing curabitur justo adipiscing amet, rutrum odio quam. Enim ligula morbi elementum proin arcu mauris sit, enim congue adipiscing. Sed pellentesque molestie proin, sit risus ipsum odio sagittis eu mauris, pellentesque ligula lorem nam duis.</p><p>Amet nibh nam elementum orci, <a href="#">eget sed</a> vivamus ornare, morbi non, mattis <b>ornare at</b> mattis enim, <i>urna in</i> justo quisque. Congue gravida congue sapien sagittis morbi rutrum porta curabitur nam eu at justo&nbsp;&mdash; ultricies risus vitae orci. Donec auctor sem rutrum et, eget, lorem, maecenas&nbsp;&mdash; non morbi quam eros at mauris&nbsp;&mdash; vitae sit vivamus integer nulla sit. Arcu sed, <b>eros sapien</b> risus: commodo auctor ligula, amet proin donec <b>odio malesuada ornare nam</b> curabitur&nbsp;&mdash; eu proin <a href="#">enim quisque cursus.</a> Magna mauris adipiscing tellus metus maecenas ipsum odio ut enim. Ut vitae, odio&nbsp;&mdash; vivamus&nbsp;&mdash; adipiscing massa lectus ut enim lorem et congue metus nec vitae. Integer fusce at eu sapien elementum magna eu amet: <i>lectus, pellentesque.</i> Metus adipiscing nibh odio curabitur bibendum auctor risus sit, maecenas lectus vitae curabitur auctor mauris fusce maecenas rutrum auctor, odio.</p> ', '', '', 1, 0, 1, '', ''),
(2, NULL, 'ru', NULL, '2017-10-17 14:15:52', '2017-10-17 14:15:52', 1, 1, 'Услуги и тарифы', 'Услуги и тарифы', 'uslugi-i-tarify', '<p>Lorem ipsum risus porttitor sed sagittis, ipsum in justo pharetra eros ligula elementum gravida arcu amet mauris auctor: mauris quisque diam nibh vivamus. Congue orci metus diam et, eget diam quisque leo duis elementum, cursus: eros bibendum <b>nam auctor.</b> Magna, tempus, enim porttitor sagittis, tempus sit in eget risus malesuada nec quisque curabitur tempus porta.</p><p>Sagittis sed magna lorem eros amet <a href="#">quisque, nec</a> tellus enim nec bibendum adipiscing bibendum molestie eu <b>morbi sed auctor</b> vitae lorem. Orci auctor vulputate odio et lectus, mauris metus ut maecenas, elementum, proin gravida, cursus molestie lorem tempus, ligula elementum morbi in sed eros nibh. Malesuada at bibendum eget curabitur nec urna eros proin donec sem tellus sem magna enim urna integer eros at nec. Morbi vulputate sagittis arcu leo ligula risus nulla <i>ipsum vivamus risus.</i></p><p>Adipiscing justo mauris lectus sodales donec ligula porta ultricies sapien a auctor amet, orci nibh sed, massa eu. Malesuada urna vitae, mauris&nbsp;&mdash; pharetra porta bibendum mauris risus quisque pellentesque eu adipiscing, molestie in fusce risus <b>ligula urna.</b> Et malesuada magna at, molestie, integer morbi, odio porta ligula <a href="#">vulputate non nam sit</a> ipsum. Massa ipsum bibendum, vivamus a tellus, justo ornare duis urna morbi: in orci, a sapien nec odio enim morbi justo.</p><p>Vitae nec ultricies adipiscing proin&nbsp;&mdash; bibendum ligula enim proin rutrum tellus mauris ultricies in nulla sed bibendum risus quam amet orci. Auctor malesuada sapien bibendum quam sagittis orci nibh, quam eu metus tellus massa ipsum proin nibh gravida nulla metus&nbsp;&mdash; leo vivamus, auctor at. Quisque metus, congue arcu urna gravida, nulla at ut elementum diam non <i>auctor vivamus sit rutrum mauris</i> nulla metus, et bibendum pharetra. Urna vitae, quam, amet justo tempus, lectus auctor duis justo et. In cursus, vivamus ligula curabitur eros nec magna, at gravida magna tempus ipsum adipiscing curabitur justo adipiscing amet, rutrum odio quam. Enim ligula morbi elementum proin arcu mauris sit, enim congue adipiscing. Sed pellentesque molestie proin, sit risus ipsum odio sagittis eu mauris, pellentesque ligula lorem nam duis.</p><p>Amet nibh nam elementum orci, <a href="#">eget sed</a> vivamus ornare, morbi non, mattis <b>ornare at</b> mattis enim, <i>urna in</i> justo quisque. Congue gravida congue sapien sagittis morbi rutrum porta curabitur nam eu at justo&nbsp;&mdash; ultricies risus vitae orci. Donec auctor sem rutrum et, eget, lorem, maecenas&nbsp;&mdash; non morbi quam eros at mauris&nbsp;&mdash; vitae sit vivamus integer nulla sit. Arcu sed, <b>eros sapien</b> risus: commodo auctor ligula, amet proin donec <b>odio malesuada ornare nam</b> curabitur&nbsp;&mdash; eu proin <a href="#">enim quisque cursus.</a> Magna mauris adipiscing tellus metus maecenas ipsum odio ut enim. Ut vitae, odio&nbsp;&mdash; vivamus&nbsp;&mdash; adipiscing massa lectus ut enim lorem et congue metus nec vitae. Integer fusce at eu sapien elementum magna eu amet: <i>lectus, pellentesque.</i> Metus adipiscing nibh odio curabitur bibendum auctor risus sit, maecenas lectus vitae curabitur auctor mauris fusce maecenas rutrum auctor, odio.</p> ', '', '', 1, 0, 2, '', ''),
(3, NULL, 'ru', NULL, '2017-10-17 14:16:05', '2017-10-17 16:05:19', 1, 1, 'Раскрытие информации', 'Раскрытие информации', 'raskrytie-informacii', '<p>Lorem ipsum risus porttitor sed sagittis, ipsum in justo pharetra eros ligula elementum gravida arcu amet mauris auctor: mauris quisque diam nibh vivamus. Congue orci metus diam et, eget diam quisque leo duis elementum, cursus: eros bibendum <strong>nam auctor.</strong> Magna, tempus, enim porttitor sagittis, tempus sit in eget risus malesuada nec quisque curabitur tempus porta.</p><p>Sagittis sed magna lorem eros amet <a href="#">quisque, nec</a> tellus enim nec bibendum adipiscing bibendum molestie eu <strong>morbi sed auctor</strong> vitae lorem. Orci auctor vulputate odio et lectus, mauris metus ut maecenas, elementum, proin gravida, cursus molestie lorem tempus, ligula elementum morbi in sed eros nibh. Malesuada at bibendum eget curabitur nec urna eros proin donec sem tellus sem magna enim urna integer eros at nec. Morbi vulputate sagittis arcu leo ligula risus nulla <i>ipsum vivamus risus.</i></p><p>Adipiscing justo mauris lectus sodales donec ligula porta ultricies sapien a auctor amet, orci nibh sed, massa eu. Malesuada urna vitae, mauris — pharetra porta bibendum mauris risus quisque pellentesque eu adipiscing, molestie in fusce risus <strong>ligula urna.</strong> Et malesuada magna at, molestie, integer morbi, odio porta ligula <a href="#">vulputate non nam sit</a> ipsum. Massa ipsum bibendum, vivamus a tellus, justo ornare duis urna morbi: in orci, a sapien nec odio enim morbi justo.</p><p>Vitae nec ultricies adipiscing proin — bibendum ligula enim proin rutrum tellus mauris ultricies in nulla sed bibendum risus quam amet orci. Auctor malesuada sapien bibendum quam sagittis orci nibh, quam eu metus tellus massa ipsum proin nibh gravida nulla metus — leo vivamus, auctor at. Quisque metus, congue arcu urna gravida, nulla at ut elementum diam non <i>auctor vivamus sit rutrum mauris</i> nulla metus, et bibendum pharetra. Urna vitae, quam, amet justo tempus, lectus auctor duis justo et. In cursus, vivamus ligula curabitur eros nec magna, at gravida magna tempus ipsum adipiscing curabitur justo adipiscing amet, rutrum odio quam. Enim ligula morbi elementum proin arcu mauris sit, enim congue adipiscing. Sed pellentesque molestie proin, sit risus ipsum odio sagittis eu mauris, pellentesque ligula lorem nam duis.</p><p>Amet nibh nam elementum orci, <a href="#">eget sed</a> vivamus ornare, morbi non, mattis <strong>ornare at</strong> mattis enim, <i>urna in</i> justo quisque. Congue gravida congue sapien sagittis morbi rutrum porta curabitur nam eu at justo — ultricies risus vitae orci. Donec auctor sem rutrum et, eget, lorem, maecenas — non morbi quam eros at mauris — vitae sit vivamus integer nulla sit. Arcu sed, <strong>eros sapien</strong> risus: commodo auctor ligula, amet proin donec <strong>odio malesuada ornare nam</strong> curabitur — eu proin <a href="#">enim quisque cursus.</a> Magna mauris adipiscing tellus metus maecenas ipsum odio ut enim. Ut vitae, odio — vivamus — adipiscing massa lectus ut enim lorem et congue metus nec vitae. Integer fusce at eu sapien elementum magna eu amet: <i>lectus, pellentesque.</i> Metus adipiscing nibh odio curabitur bibendum auctor risus sit, maecenas lectus vitae curabitur auctor mauris fusce maecenas rutrum auctor, odio.</p>', '', '', 1, 0, 3, '', '_list'),
(4, NULL, 'ru', 3, '2017-10-17 16:03:42', '2017-10-17 16:03:42', 1, 1, 'Общая информация об управляющей организации', 'Общая информация об управляющей организации', 'obshchaya-informaciya-ob-upravlyayushchey-organizacii', '<p>Общая информация об управляющей организации</p>', '', '', 1, 0, 4, '', ''),
(5, NULL, 'ru', 3, '2017-10-17 16:03:56', '2017-10-17 16:03:56', 1, 1, 'Основные показатели финансово-хозяйственной деятельности', 'Основные показатели финансово-хозяйственной деятельности', 'osnovnye-pokazateli-finansovo-hozyaystvennoy-deyatelnosti', '<p>Основные показатели финансово-хозяйственной деятельности</p>', '', '', 1, 0, 5, '', ''),
(6, NULL, 'ru', 3, '2017-10-17 16:04:14', '2017-10-17 16:04:14', 1, 1, 'Информация о выполняемых работах (оказываемых услугах)', 'Информация о выполняемых работах (оказываемых услугах)', 'informaciya-o-vypolnyaemyh-rabotah-okazyvaemyh-uslugah', '<p>Информация о выполняемых работах (оказываемых услугах)</p>', '', '', 1, 0, 6, '', ''),
(7, NULL, 'ru', 3, '2017-10-17 16:04:27', '2017-10-17 16:04:27', 1, 1, 'Информация о порядке и условиях оказания услуг по содержанию и ремонту общего имущества', 'Информация о порядке и условиях оказания услуг по содержанию и ремонту общего имущества', 'informaciya-o-poryadke-i-usloviyah-okazaniya-uslug-po-soderzhaniyu-i-remontu-obshchego-imushchestva', '<p> Информация о порядке и условиях оказания услуг по содержанию и ремонту общего имущества</p>', '', '', 1, 0, 7, '', ''),
(8, NULL, 'ru', 3, '2017-10-17 16:04:42', '2017-10-17 16:04:42', 1, 1, 'Сведения о нарушениях качества содержания и ремонта общего имущества и привлечения к ответственности', 'Сведения о нарушениях качества содержания и ремонта общего имущества и привлечения к ответственности', 'svedeniya-o-narusheniyah-kachestva-soderzhaniya-i-remonta-obshchego-imushchestva-i-privlecheniya-k-otvetstvennosti', '<p>Сведения о нарушениях качества содержания и ремонта общего имущества и привлечения к ответственности</p>', '', '', 1, 0, 8, '', ''),
(9, NULL, 'ru', 3, '2017-10-17 16:05:00', '2017-10-17 16:05:00', 1, 1, 'Информация о периодичности, либо дате выполнения работ (услуг) управляющей организации', 'Информация о периодичности, либо дате выполнения работ (услуг) управляющей организации', 'informaciya-o-periodichnosti-libo-date-vypolneniya-rabot-uslug-upravlyayushchey-organizacii', '<p>Информация о периодичности, либо дате выполнения работ (услуг) управляющей организации</p>', '', '', 1, 0, 9, '', ''),
(10, NULL, 'ru', NULL, '2017-10-17 21:32:11', '2017-10-17 21:32:48', 1, 1, 'Об управляющей компании', 'Об управляющей компании', 'ob-upravlyayushchey-kompanii', '<p>Lorem ipsum risus porttitor sed sagittis, ipsum in justo pharetra eros ligula elementum gravida arcu amet mauris auctor: mauris quisque diam nibh vivamus. Congue orci metus diam et, eget diam quisque leo duis elementum, cursus: eros bibendum <strong>nam auctor.</strong> Magna, tempus, enim porttitor sagittis, tempus sit in eget risus malesuada nec quisque curabitur tempus porta.\r\n</p><p>Sagittis sed magna lorem eros amet <a href="#">quisque, nec</a> tellus enim nec bibendum adipiscing bibendum molestie eu <strong>morbi sed auctor</strong> vitae lorem. Orci auctor vulputate odio et lectus, mauris metus ut maecenas, elementum, proin gravida, cursus molestie lorem tempus, ligula elementum morbi in sed eros nibh. Malesuada at bibendum eget curabitur nec urna eros proin donec sem tellus sem magna enim urna integer eros at nec. Morbi vulputate sagittis arcu leo ligula risus nulla <i>ipsum vivamus risus.</i>\r\n</p><p>Adipiscing justo mauris lectus sodales donec ligula porta ultricies sapien a auctor amet, orci nibh sed, massa eu. Malesuada urna vitae, mauris — pharetra porta bibendum mauris risus quisque pellentesque eu adipiscing, molestie in fusce risus <strong>ligula urna.</strong> Et malesuada magna at, molestie, integer morbi, odio porta ligula <a href="#">vulputate non nam sit</a> ipsum. Massa ipsum bibendum, vivamus a tellus, justo ornare duis urna morbi: in orci, a sapien nec odio enim morbi justo.\r\n</p><p>Vitae nec ultricies adipiscing proin — bibendum ligula enim proin rutrum tellus mauris ultricies in nulla sed bibendum risus quam amet orci. Auctor malesuada sapien bibendum quam sagittis orci nibh, quam eu metus tellus massa ipsum proin nibh gravida nulla metus — leo vivamus, auctor at. Quisque metus, congue arcu urna gravida, nulla at ut elementum diam non <i>auctor vivamus sit rutrum mauris</i> nulla metus, et bibendum pharetra. Urna vitae, quam, amet justo tempus, lectus auctor duis justo et. In cursus, vivamus ligula curabitur eros nec magna, at gravida magna tempus ipsum adipiscing curabitur justo adipiscing amet, rutrum odio quam. Enim ligula morbi elementum proin arcu mauris sit, enim congue adipiscing. Sed pellentesque molestie proin, sit risus ipsum odio sagittis eu mauris, pellentesque ligula lorem nam duis.\r\n</p><p>Amet nibh nam elementum orci, <a href="#">eget sed</a> vivamus ornare, morbi non, mattis <strong>ornare at</strong> mattis enim, <i>urna in</i> justo quisque. Congue gravida congue sapien sagittis morbi rutrum porta curabitur nam eu at justo — ultricies risus vitae orci. Donec auctor sem rutrum et, eget, lorem, maecenas — non morbi quam eros at mauris — vitae sit vivamus integer nulla sit. Arcu sed, <strong>eros sapien</strong> risus: commodo auctor ligula, amet proin donec <strong>odio malesuada ornare nam</strong> curabitur — eu proin <a href="#">enim quisque cursus.</a> Magna mauris adipiscing tellus metus maecenas ipsum odio ut enim. Ut vitae, odio — vivamus — adipiscing massa lectus ut enim lorem et congue metus nec vitae. Integer fusce at eu sapien elementum magna eu amet: <i>lectus, pellentesque.</i> Metus adipiscing nibh odio curabitur bibendum auctor risus sit, maecenas lectus vitae curabitur auctor mauris fusce maecenas rutrum auctor, odio.\r\n</p>', '', '', 1, 0, 10, '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `yupe_sitemap_page`
--

CREATE TABLE IF NOT EXISTS `yupe_sitemap_page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(250) NOT NULL,
  `changefreq` varchar(20) NOT NULL,
  `priority` float NOT NULL DEFAULT '0.5',
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_sitemap_page_url` (`url`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `yupe_sitemap_page`
--

INSERT INTO `yupe_sitemap_page` (`id`, `url`, `changefreq`, `priority`, `status`) VALUES
(1, '/', 'daily', 0.5, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `yupe_user_tokens`
--

CREATE TABLE IF NOT EXISTS `yupe_user_tokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `token` varchar(255) DEFAULT NULL,
  `type` int(11) NOT NULL,
  `status` int(11) DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `expire_time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ix_yupe_user_tokens_user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `yupe_user_tokens`
--

INSERT INTO `yupe_user_tokens` (`id`, `user_id`, `token`, `type`, `status`, `create_time`, `update_time`, `ip`, `expire_time`) VALUES
(1, 1, '6a6EykF8b8zP1V8I_52H8XxllWkOcxeF', 4, 0, '2017-10-17 18:14:19', '2017-10-17 18:14:19', '5.59.32.190', '2017-10-24 18:14:19');

-- --------------------------------------------------------

--
-- Структура таблицы `yupe_user_user`
--

CREATE TABLE IF NOT EXISTS `yupe_user_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `update_time` datetime NOT NULL,
  `first_name` varchar(250) DEFAULT NULL,
  `middle_name` varchar(250) DEFAULT NULL,
  `last_name` varchar(250) DEFAULT NULL,
  `nick_name` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `gender` tinyint(1) NOT NULL DEFAULT '0',
  `birth_date` date DEFAULT NULL,
  `site` varchar(250) NOT NULL DEFAULT '',
  `about` varchar(250) NOT NULL DEFAULT '',
  `location` varchar(250) NOT NULL DEFAULT '',
  `status` int(11) NOT NULL DEFAULT '2',
  `access_level` int(11) NOT NULL DEFAULT '0',
  `visit_time` datetime DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `avatar` varchar(150) DEFAULT NULL,
  `hash` varchar(255) NOT NULL DEFAULT '0c78521f8419a6152f4d2ae37c8571e60.13532300 1508237901',
  `email_confirm` tinyint(1) NOT NULL DEFAULT '0',
  `phone` char(30) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_user_user_nick_name` (`nick_name`),
  UNIQUE KEY `ux_yupe_user_user_email` (`email`),
  KEY `ix_yupe_user_user_status` (`status`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `yupe_user_user`
--

INSERT INTO `yupe_user_user` (`id`, `update_time`, `first_name`, `middle_name`, `last_name`, `nick_name`, `email`, `gender`, `birth_date`, `site`, `about`, `location`, `status`, `access_level`, `visit_time`, `create_time`, `avatar`, `hash`, `email_confirm`, `phone`) VALUES
(1, '2017-10-17 14:01:23', '', '', '', 'admin', 'sadreev-kzn@ya.ru', 0, NULL, '', '', '', 1, 1, '2017-10-17 18:14:19', '2017-10-17 14:01:23', NULL, '$2y$13$2zmHCEgbLs5SL4Jt2CrQV.vIZI2HQVUyS3EffWWIS/4s16IHI/Yoy', 1, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `yupe_user_user_auth_assignment`
--

CREATE TABLE IF NOT EXISTS `yupe_user_user_auth_assignment` (
  `itemname` char(64) NOT NULL,
  `userid` int(11) NOT NULL,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`itemname`,`userid`),
  KEY `fk_yupe_user_user_auth_assignment_user` (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `yupe_user_user_auth_assignment`
--

INSERT INTO `yupe_user_user_auth_assignment` (`itemname`, `userid`, `bizrule`, `data`) VALUES
('admin', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `yupe_user_user_auth_item`
--

CREATE TABLE IF NOT EXISTS `yupe_user_user_auth_item` (
  `name` char(64) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`name`),
  KEY `ix_yupe_user_user_auth_item_type` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `yupe_user_user_auth_item`
--

INSERT INTO `yupe_user_user_auth_item` (`name`, `type`, `description`, `bizrule`, `data`) VALUES
('admin', 2, 'Администратор', NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `yupe_user_user_auth_item_child`
--

CREATE TABLE IF NOT EXISTS `yupe_user_user_auth_item_child` (
  `parent` char(64) NOT NULL,
  `child` char(64) NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `fk_yupe_user_user_auth_item_child_child` (`child`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `yupe_yupe_settings`
--

CREATE TABLE IF NOT EXISTS `yupe_yupe_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module_id` varchar(100) NOT NULL,
  `param_name` varchar(100) NOT NULL,
  `param_value` varchar(500) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `type` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_yupe_settings_module_id_param_name_user_id` (`module_id`,`param_name`,`user_id`),
  KEY `ix_yupe_yupe_settings_module_id` (`module_id`),
  KEY `ix_yupe_yupe_settings_param_name` (`param_name`),
  KEY `fk_yupe_yupe_settings_user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=27 ;

--
-- Дамп данных таблицы `yupe_yupe_settings`
--

INSERT INTO `yupe_yupe_settings` (`id`, `module_id`, `param_name`, `param_value`, `create_time`, `update_time`, `user_id`, `type`) VALUES
(1, 'yupe', 'siteDescription', 'Мы работаем ради того, чтобы Вы чувствовали себя максимально комфортно. Для нас очень важно Ваше спокойствие. Мы бережно относимся к окружающей среде и обеспечиваем порядок на территории. Кроме этого, мы делаем жизнь в наших домах уютной и разнообразной', '2017-10-17 14:01:45', '2017-10-17 21:47:55', 1, 1),
(2, 'yupe', 'siteName', 'Управляющая компания Оазис', '2017-10-17 14:01:45', '2017-10-17 14:01:45', 1, 1),
(3, 'yupe', 'siteKeyWords', 'Управляющая компания Оазис', '2017-10-17 14:01:45', '2017-10-17 14:01:45', 1, 1),
(4, 'yupe', 'email', 'sadreev-kzn@ya.ru', '2017-10-17 14:01:45', '2017-10-17 14:01:45', 1, 1),
(5, 'yupe', 'theme', 'oasis', '2017-10-17 14:01:45', '2017-10-17 14:07:04', 1, 1),
(6, 'yupe', 'backendTheme', '', '2017-10-17 14:01:45', '2017-10-17 14:01:45', 1, 1),
(7, 'yupe', 'defaultLanguage', 'ru', '2017-10-17 14:01:45', '2017-10-17 14:01:45', 1, 1),
(8, 'yupe', 'defaultBackendLanguage', 'ru', '2017-10-17 14:01:45', '2017-10-17 14:01:45', 1, 1),
(9, 'yupe', 'coreCacheTime', '3600', '2017-10-17 14:07:04', '2017-10-17 14:07:04', 1, 1),
(10, 'yupe', 'uploadPath', 'uploads', '2017-10-17 14:07:04', '2017-10-17 14:07:04', 1, 1),
(11, 'yupe', 'editor', 'redactor', '2017-10-17 14:07:04', '2017-10-17 14:07:04', 1, 1),
(12, 'yupe', 'availableLanguages', 'ru', '2017-10-17 14:07:04', '2017-10-17 14:37:53', 1, 1),
(13, 'yupe', 'allowedIp', '', '2017-10-17 14:07:04', '2017-10-17 14:07:04', 1, 1),
(14, 'yupe', 'hidePanelUrls', '0', '2017-10-17 14:07:04', '2017-10-17 14:07:04', 1, 1),
(15, 'yupe', 'logo', 'images/logo.png', '2017-10-17 14:07:04', '2017-10-17 14:07:04', 1, 1),
(16, 'yupe', 'allowedExtensions', 'gif, jpeg, png, jpg, zip, rar, doc, docx, xls, xlsx, pdf', '2017-10-17 14:07:04', '2017-10-17 14:07:04', 1, 1),
(17, 'yupe', 'mimeTypes', 'image/gif,image/jpeg,image/png,application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document, application/zip,application/x-rar,application/x-rar-compressed, application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', '2017-10-17 14:07:04', '2017-10-17 14:07:04', 1, 1),
(18, 'yupe', 'maxSize', '5242880', '2017-10-17 14:07:04', '2017-10-17 14:07:04', 1, 1),
(19, 'news', 'editor', 'redactor', '2017-10-17 21:17:56', '2017-10-17 21:17:56', 1, 1),
(20, 'news', 'mainCategory', '1', '2017-10-17 21:17:56', '2017-10-17 21:17:56', 1, 1),
(21, 'news', 'uploadPath', 'news', '2017-10-17 21:17:56', '2017-10-17 21:17:56', 1, 1),
(22, 'news', 'allowedExtensions', 'jpg,jpeg,png,gif', '2017-10-17 21:17:56', '2017-10-17 21:17:56', 1, 1),
(23, 'news', 'minSize', '0', '2017-10-17 21:17:56', '2017-10-17 21:17:56', 1, 1),
(24, 'news', 'maxSize', '5368709120', '2017-10-17 21:17:56', '2017-10-17 21:17:56', 1, 1),
(25, 'news', 'rssCount', '10', '2017-10-17 21:17:56', '2017-10-17 21:17:56', 1, 1),
(26, 'news', 'perPage', '10', '2017-10-17 21:17:56', '2017-10-17 21:17:56', 1, 1);

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `yupe_category_category`
--
ALTER TABLE `yupe_category_category`
  ADD CONSTRAINT `fk_yupe_category_category_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `yupe_category_category` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `yupe_dictionary_dictionary_data`
--
ALTER TABLE `yupe_dictionary_dictionary_data`
  ADD CONSTRAINT `fk_yupe_dictionary_dictionary_data_create_user_id` FOREIGN KEY (`create_user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_yupe_dictionary_dictionary_data_data_group_id` FOREIGN KEY (`group_id`) REFERENCES `yupe_dictionary_dictionary_group` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_yupe_dictionary_dictionary_data_update_user_id` FOREIGN KEY (`update_user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `yupe_dictionary_dictionary_group`
--
ALTER TABLE `yupe_dictionary_dictionary_group`
  ADD CONSTRAINT `fk_yupe_dictionary_dictionary_group_create_user_id` FOREIGN KEY (`create_user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_yupe_dictionary_dictionary_group_update_user_id` FOREIGN KEY (`update_user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `yupe_feedback_feedback`
--
ALTER TABLE `yupe_feedback_feedback`
  ADD CONSTRAINT `fk_yupe_feedback_feedback_answer_user` FOREIGN KEY (`answer_user`) REFERENCES `yupe_user_user` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_yupe_feedback_feedback_category` FOREIGN KEY (`category_id`) REFERENCES `yupe_category_category` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `yupe_gallery_gallery`
--
ALTER TABLE `yupe_gallery_gallery`
  ADD CONSTRAINT `fk_yupe_gallery_gallery_gallery_to_category` FOREIGN KEY (`category_id`) REFERENCES `yupe_category_category` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_yupe_gallery_gallery_gallery_preview_to_image` FOREIGN KEY (`preview_id`) REFERENCES `yupe_image_image` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_yupe_gallery_gallery_owner` FOREIGN KEY (`owner`) REFERENCES `yupe_user_user` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `yupe_gallery_image_to_gallery`
--
ALTER TABLE `yupe_gallery_image_to_gallery`
  ADD CONSTRAINT `fk_yupe_gallery_image_to_gallery_gallery_to_image_gallery` FOREIGN KEY (`gallery_id`) REFERENCES `yupe_gallery_gallery` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_yupe_gallery_image_to_gallery_gallery_to_image_image` FOREIGN KEY (`image_id`) REFERENCES `yupe_image_image` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `yupe_image_image`
--
ALTER TABLE `yupe_image_image`
  ADD CONSTRAINT `fk_yupe_image_image_category_id` FOREIGN KEY (`category_id`) REFERENCES `yupe_category_category` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_yupe_image_image_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `yupe_image_image` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_yupe_image_image_user_id` FOREIGN KEY (`user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `yupe_mail_mail_template`
--
ALTER TABLE `yupe_mail_mail_template`
  ADD CONSTRAINT `fk_yupe_mail_mail_template_event_id` FOREIGN KEY (`event_id`) REFERENCES `yupe_mail_mail_event` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `yupe_menu_menu_item`
--
ALTER TABLE `yupe_menu_menu_item`
  ADD CONSTRAINT `fk_yupe_menu_menu_item_menu_id` FOREIGN KEY (`menu_id`) REFERENCES `yupe_menu_menu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `yupe_news_news`
--
ALTER TABLE `yupe_news_news`
  ADD CONSTRAINT `fk_yupe_news_news_category_id` FOREIGN KEY (`category_id`) REFERENCES `yupe_category_category` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_yupe_news_news_user_id` FOREIGN KEY (`user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `yupe_notify_settings`
--
ALTER TABLE `yupe_notify_settings`
  ADD CONSTRAINT `fk_yupe_notify_settings_user_id` FOREIGN KEY (`user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `yupe_page_page`
--
ALTER TABLE `yupe_page_page`
  ADD CONSTRAINT `fk_yupe_page_page_category_id` FOREIGN KEY (`category_id`) REFERENCES `yupe_category_category` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_yupe_page_page_change_user_id` FOREIGN KEY (`change_user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_yupe_page_page_user_id` FOREIGN KEY (`user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `yupe_user_tokens`
--
ALTER TABLE `yupe_user_tokens`
  ADD CONSTRAINT `fk_yupe_user_tokens_user_id` FOREIGN KEY (`user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `yupe_user_user_auth_assignment`
--
ALTER TABLE `yupe_user_user_auth_assignment`
  ADD CONSTRAINT `fk_yupe_user_user_auth_assignment_item` FOREIGN KEY (`itemname`) REFERENCES `yupe_user_user_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_yupe_user_user_auth_assignment_user` FOREIGN KEY (`userid`) REFERENCES `yupe_user_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `yupe_user_user_auth_item_child`
--
ALTER TABLE `yupe_user_user_auth_item_child`
  ADD CONSTRAINT `fk_yupe_user_user_auth_itemchild_parent` FOREIGN KEY (`parent`) REFERENCES `yupe_user_user_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_yupe_user_user_auth_item_child_child` FOREIGN KEY (`child`) REFERENCES `yupe_user_user_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `yupe_yupe_settings`
--
ALTER TABLE `yupe_yupe_settings`
  ADD CONSTRAINT `fk_yupe_yupe_settings_user_id` FOREIGN KEY (`user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION;
SET FOREIGN_KEY_CHECKS=1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
