<?php

/**
 * Виджет вывода последних новостей
 *
 * @category YupeWidget
 * @package  yupe.modules.news.widgets
 * @author   YupeTeam <team@yupe.ru>
 * @license  BSD http://ru.wikipedia.org/wiki/%D0%9B%D0%B8%D1%86%D0%B5%D0%BD%D0%B7%D0%B8%D1%8F_BSD
 * @version  0.5.3
 * @link     http://yupe.ru
 *
 **/
Yii::import('application.modules.dictionary.models.*');

class ContactDictionaryWidget extends yupe\widgets\YWidget
{
    /** @var $categories mixed Список категорий, из которых выбирать новости. NULL - все */
    public $code = null;

    /**
     * @var string
     */
    public $view = 'view';

    /**
     * @throws CException
     */
    public function run()
    {

        $data = DictionaryData::model()->findAll(
            [
                'condition' => 'group_id = :group_id',
                'params' => [':group_id' => 2],
                'order' => 'name DESC',
            ]);


        $this->render($this->view, ['data' => $data]);
    }
}
