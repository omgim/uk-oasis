<?php

/**
 * Виджет вывода последних новостей
 *
 * @category YupeWidget
 * @package  yupe.modules.news.widgets
 * @author   YupeTeam <team@yupe.ru>
 * @license  BSD http://ru.wikipedia.org/wiki/%D0%9B%D0%B8%D1%86%D0%B5%D0%BD%D0%B7%D0%B8%D1%8F_BSD
 * @version  0.5.3
 * @link     http://yupe.ru
 *
 **/
Yii::import('application.modules.news.models.*');

/**
 * Class LastNewsWidget
 */
class DebtNewsWidget extends yupe\widgets\YWidget
{
    /** @var $categories mixed Список категорий, из которых выбирать новости. NULL - все */
    public $categories = null;

    /**
     * @var string
     */
    public $view = 'view';

    /**
     * @throws CException
     */
    public function run()
    {

        $data = News::model()->published()->findAll(
            [
                'condition' => 'category_id = :category_id',
                'params' => [':category_id' => 2],
                'limit' => 1,
                'order' => 'update_time DESC'
            ]);


        $this->render($this->view, ['models' => $data]);
    }
}
