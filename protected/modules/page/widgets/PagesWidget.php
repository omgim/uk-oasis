<?php
/**
 * PagesWidget виджет для вывода страниц
 *
 * @author yupe team <team@yupe.ru>
 * @link http://yupe.ru
 * @copyright 2009-2013 amyLabs && Yupe! team
 * @package yupe.modules.page.widgets
 * @since 0.1
 *
 */
Yii::import('application.modules.page.models.*');

/**
 * Class PagesWidget
 */
class PagesWidget extends yupe\widgets\YWidget
{
    /**
     * @var
     */
    public $pageStatus;
    /**
     * @var bool
     */
    public $topLevelOnly = false;
    /**
     * @var string
     */
    public $order = 't.order ASC, t.create_time ASC';
    /**
     * @var
     */
    public $parent_id;
    /**
     * @var string
     */
    public $view = 'view';
    /**
     * @var bool
     */
    public $visible = true;

    /**
     *
     */
    public function init()
    {
        parent::init();

        if (!$this->pageStatus) {
            $this->pageStatus = Page::STATUS_PUBLISHED;
        }

        $this->parent_id = (int)$this->parent_id;
    }

    /**
     * @throws CException
     */
    public function run()
    {
        if ($this->visible) {
            $criteria = new CDbCriteria();
            $criteria->order = $this->order;
            $criteria->addCondition("status = {$this->pageStatus}");


            if ($this->parent_id) {
                $criteria->addCondition("parent_id = {$this->parent_id}");
            }

            $this->render(
                $this->view,
                [
                    'pages' => Page::model()->findAll($criteria),
                ]
            );
        }
    }
}
