$(document).ready(function () {
    $(".popupbutton").fancybox({
        padding: 37,
        overlayOpacity: 0.87,
        overlayColor: "#fff",
        transitionIn: "none",
        transitionOut: "none",
        titlePosition: "inside",
        centerOnScroll: true,
        maxWidth: 400,
        minHeight: 310
    });
    $(".burger-wrap").click(function () {
        $(this).children().toggleClass("active");
        $(".nav").toggleClass("active");
        return false;
    });
    $(".info-item-head").click(function () {
        var content = $(this).next();
        var parent = $(this).parent();
        if (parent.hasClass("active")) {
            content.slideUp();
            parent.removeClass("active");
        } else {
            $(".info-item-content").slideUp();
            $(".info-item").removeClass("active");
            content.slideDown();
            parent.addClass("active");
        }
    });
});