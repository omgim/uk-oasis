<?php
/**
 * @var Callback $model
 * @var string $phoneMask
 * @var CActiveForm $form
 */
?>

<div id="popupform">
    <h2>Обратная связь</h2>
    <div class="comment">Ввести показания счетчиков электроэнергии, горячей и холодной воды</div>


    <?php $form = $this->beginWidget('CActiveForm', [
        'id' => 'callback-form',
        'action' => Yii::app()->createUrl('/callback/callback/send'),
        'enableClientValidation' => true,
        'clientOptions' => [
            'validateOnSubmit' => true,
            'afterValidate' => 'js:callbackSendForm',
        ],
        'htmlOptions' => [
            'class' => 'modal-form',
        ]
    ]); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->textField($model, 'name', ['class' => 'input_text', 'placeholder' => 'Ваше имя', 'onfocus' => 'placeholder=\'\';', 'onblur' => 'placeholder=\'Ваше имя\';']); ?>


    <?php $this->widget('CMaskedTextField', [
        'model' => $model,
        'attribute' => 'phone',
        'mask' => $phoneMask,
        'htmlOptions' => [
            'placeholder' => 'Номер телефона',
            'onfocus' => 'placeholder=\'\';',
            'onblur' => 'Номер телефона\';',
            'class' => 'input_text'
        ]
    ]); ?>
    <?= $form->error($model, 'phone') ?>


    <?= $form->textArea($model, 'comment', ['class' => 'textarea_text', 'placeholder' => 'Сообщение', 'onfocus' => 'placeholder=\'\';', 'onblur' => 'placeholder=\'Сообщение\';']); ?>


    <button type="submit" id="callback-send" class="button"> Отправить заявку</button>


    <?php $this->endWidget(); ?>

    <span class="under-form">Мы не занимаемся рассылкой рекламных сообщений, а так же не передаем контактные данные третьим лицам.</span>
</div>