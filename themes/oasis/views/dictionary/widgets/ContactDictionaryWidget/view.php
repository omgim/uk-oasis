<?php
Yii::import('application.modules.dictionary.*');

$b = false;

?>


<?php if (isset($data) && $data != []): ?>
    <div class="contacts-item">
    <?php foreach ($data as $item => $d): ?>

        <?php

        if ((!(($item) < ceil(count($data) / 2))) and !$b) {
            $b = true; ?>
            </div>
            <div class="contacts-item <?= $item ?>">
        <? } ?>

        <div class="contacts-item-par">
            <b><?= $d->name ?></b>
            <div class="contacts-item-content">
                <p><?= $d->value ?></p>

                <?php if (!empty($d->phone)) { ?>
                    <a href="tel:<?= $d->phone ?>" class="contacts-item-content__tel"><?= $d->phone ?></a>
                <?php } ?>

            </div>
        </div>

    <?php endforeach; ?>
    </div>

<?php endif; ?>
