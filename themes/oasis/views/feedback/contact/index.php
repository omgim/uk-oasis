<?php
/**
 * @var CActiveForm $form
 */
$this->title = Yii::t('FeedbackModule.feedback', 'Contacts');
$this->breadcrumbs = [Yii::t('FeedbackModule.feedback', 'Contacts')];
Yii::import('application.modules.feedback.FeedbackModule');
Yii::import('application.modules.install.InstallModule');
?>
<div class="contacts-wrap">
    <div class="container">
        <div class="contacts">
            <div class="head news-head">
                <p>Контакты</p>
            </div>
            <div class="contacts-items">

                <?php $this->widget('application.modules.dictionary.widgets.ContactDictionaryWidget', ['code' => 'spravochnik-dlya-kontaktov']); ?>

            </div>
        </div>
    </div>
</div>

<!-- Map -->

<div class="map">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15987.45932634925!2d49.12645738773411!3d55.790380957648495!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x415ead2b7caccd99%3A0x7fcb77b9b5ad8c65!2z0JrQsNC30LDQvdGMLCDQoNC10YHQvy4g0KLQsNGC0LDRgNGB0YLQsNC9!5e0!3m2!1sru!2sru!4v1507633797374"
            allowfullscreen></iframe>
</div>
