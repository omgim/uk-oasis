<div id="cookie-notice" role="banner" class="cn-bottom"
     style="color: rgb(255, 255, 255); background-color: rgb(0, 0, 0); display: none;">
    <div class="cookie-notice-container"><span id="cn-notice-text">Для наилучшего представления сайта мы используем файлы Cookie. Если Вы продолжите использовать сайт, мы будем считать что Вы с этим согласны, в противном случае покиньте данный сайт.</span>
        <a href="#" id="cn-accept-cookie" data-cookie-set="accept" class="cn-set-cookie button">Да, меня все
            устраивает!</a>
    </div>
</div>