<div class="footer-wrap">
    <div class="container">
        <div class="footer cf">
            <div class="logo-wrap">
                <a href="/" class="logo">
                    <img src="/web/images/footer-logo.png" alt="">
                </a>
                <b>Управляющая компания</b>
            </div>
            <div class="header-items">
                <div class="header-item">
                    <b><?php $this->widget('application.modules.dictionary.widgets.GetDictionaryDataWidget', ['code' => 'adres', 'view' => 'simple']); ?></b>
                    <span><?php $this->widget('application.modules.dictionary.widgets.GetDictionaryDataWidget', ['code' => 'vremya-raboty', 'view' => 'simple']); ?></span>
                </div>
                <div class="header-item">
                    <span><?php $this->widget('application.modules.dictionary.widgets.GetDictionaryDataWidget', ['code' => 'glavnyy-telefon', 'view' => 'phone']); ?></span>
                    <span>Диспетчерская служба</span>
                </div>
            </div>
            <div class="link header-link">
                <?php $this->widget('application.modules.news.widgets.DebtNewsWidget'); ?>
            </div>
        </div>
    </div>
</div>

<?php $this->widget('application.modules.callback.widgets.CallbackWidget'); ?>