<div class="header-wrap">
    <div class="container">
        <div class="header cf">
            <div class="logo-wrap">
                <a href="/" class="logo" title="Управляющая компания Оазис">
                    <img src="/web/images/logo.png" alt="Управляющая компания Оазис" title="Управляющая компания Оазис">
                </a>
                <b>Управляющая компания</b>
            </div>
            <div class="header-items">
                <div class="header-item">
                    <b><?php $this->widget('application.modules.dictionary.widgets.GetDictionaryDataWidget', ['code' => 'adres', 'view' => 'simple']); ?></b>
                    <span><?php $this->widget('application.modules.dictionary.widgets.GetDictionaryDataWidget', ['code' => 'vremya-raboty', 'view' => 'simple']); ?></span>
                </div>
                <div class="header-item">
                    <span><?php $this->widget('application.modules.dictionary.widgets.GetDictionaryDataWidget', ['code' => 'glavnyy-telefon', 'view' => 'phone']); ?></span>
                    <span>Диспетчерская служба</span>
                </div>
            </div>
            <div class="link header-link">
                <?php $this->widget('application.modules.news.widgets.DebtNewsWidget'); ?>
            </div>
        </div>
    </div>
</div>

<!-- Nav -->

<div class="nav-wrap">
    <div class="container">
        <div class="burger-wrap">
            <a href="#"><span></span></a>
        </div>
        <div class="nav">

            <?php if (Yii::app()->hasModule('menu')): ?>
                <?php $this->widget('application.modules.menu.widgets.MainMenuWidget', [
                    'name' => 'main-menu',
                    'layout' => 'view'
                ]); ?>
            <?php endif; ?>

        </div>
    </div>
</div>