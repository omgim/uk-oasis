<!DOCTYPE html>
<html lang="<?= Yii::app()->getLanguage(); ?>">

<head>
    <?php
    \yupe\components\TemplateEvent::fire(OasisThemeEvents::HEAD_START);

    Yii::app()->clientScript->coreScriptPosition = CClientScript::POS_END;

    Yii::app()->getClientScript()->registerCssFile('/web/lib/fancybox/jquery.fancybox.css');

    Yii::app()->getClientScript()->registerCssFile('/web/css/slick.css');
    Yii::app()->getClientScript()->registerCssFile('/web/css/fonts.css');
    Yii::app()->getClientScript()->registerCssFile('/web/css/style.css');
    Yii::app()->getClientScript()->registerCssFile('/web/css/media.css');
    Yii::app()->getClientScript()->registerCssFile('/web/css/cookie.css');

    Yii::app()->getClientScript()->registerCoreScript('jquery');

    ?>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width">

    <title><?= $this->title; ?></title>
    <meta name="description" content="<?= $this->description; ?>"/>
    <meta name="keywords" content="<?= $this->keywords; ?>"/>
    <?php if ($this->canonical): ?>
        <link rel="canonical" href="<?= $this->canonical ?>"/>
    <?php endif; ?>

    <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <meta name="yandex-verification" content="84dd5de4d20022a0" />


    <script type="text/javascript">
        var yupeTokenName = '<?= Yii::app()->getRequest()->csrfTokenName;?>';
        var yupeToken = '<?= Yii::app()->getRequest()->getCsrfToken();?>';
    </script>
    <?php \yupe\components\TemplateEvent::fire(OasisThemeEvents::HEAD_END); ?>
</head>

<body>
<?php \yupe\components\TemplateEvent::fire(OasisThemeEvents::BODY_START); ?>

<?php if (Yii::app()->hasModule('contentblock')): ?>
    <?php $this->widget(
        "application.modules.contentblock.widgets.ContentBlockWidget",
        ["code" => "ya-metrika"]
    ); ?>
<?php endif; ?>

<?php $this->renderPartial('//layouts/_header'); ?>

<?= $content ?>

<?php $this->renderPartial('//layouts/_footer'); ?>
<?php $this->renderPartial('//layouts/_cookie'); ?>

<?php \yupe\components\TemplateEvent::fire(OasisThemeEvents::BODY_END); ?>
<div class='notifications top-right' id="notifications"></div>
<?php
Yii::app()->getClientScript()->registerScriptFile('/web/js/slick.min.js', CClientScript::POS_END);
Yii::app()->getClientScript()->registerScriptFile('/web/lib/fancybox/jquery.fancybox.pack.js', CClientScript::POS_END);
Yii::app()->getClientScript()->registerScriptFile('/web/js/common.js', CClientScript::POS_END);
Yii::app()->getClientScript()->registerScriptFile('/web/js/cookie.js', CClientScript::POS_END);
?>
</body>
</html>