<ul>
    <?php foreach ($this->params['items'] as $item): ?>
        <?php if ($item['visible']): ?>

            <li>
                <?= CHtml::link(CHtml::encode($item['label']), $item['url'], ['title' => $item['label']]); ?>
            </li>

        <?php endif; ?>
    <?php endforeach; ?>
</ul>
