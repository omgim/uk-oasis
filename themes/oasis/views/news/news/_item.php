<?php
/* @var $data News */
$url = Yii::app()->createUrl('/news/news/view', ['slug' => $data->slug]);
?>

<div class="main-news-item">
    <img src="/web/images/news.png" alt="">
    <div class="main-news-content">
        <span><?= Yii::app()->getDateFormatter()->formatDateTime(
                $data->date,
                "long",
                false
            ); ?></span>
        <a href="<?=$url ?>" class="main-news__head" title="<?=$data->title ?>"><?=$data->title ?></a>
        <p><?= $data->getQuote() ?></p>
    </div>
</div>