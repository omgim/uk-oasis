<?php
$this->title = Yii::t('NewsModule.news', 'News');
$this->breadcrumbs = [Yii::t('NewsModule.news', 'News')];
?>


<div class="news-wrap">
    <div class="container">
        <div class="news">
            <div class="head news-head">
                <p>Новости для жителей</p>
            </div>

            <?php
            $this->widget('zii.widgets.CListView', array(
                'dataProvider' => $dataProvider,
                'itemView' => '_item',
                'ajaxUpdate' => false,
                'itemsTagName' => 'div',
                'itemsCssClass' => 'main-news-items ',
                'template' => "{items}",
            ));
            ?>
            <div class="main-news-items">
                <div class="pagination">
                    <?php
                    $dataProvider->getData();
                    $pages = $dataProvider->getPagination();
                    $this->widget('CLinkPager', array(
                        'pages' => $pages,
                        'firstPageLabel' => false,
                        'header' => false,
                        'prevPageLabel' => '<img src="/web/images/pagination__arrow1.png" alt="">',
                        'previousPageCssClass' => 'pagination__arrows',
                        'nextPageCssClass' => 'pagination__arrows',
                        'nextPageLabel' => '<img src="/web/images/pagination__arrow2.png" alt="">',
                        'lastPageLabel' => false,
                        'maxButtonCount' => 5,
                        'selectedPageCssClass' => 'active',
                        'htmlOptions' => array(
                            'class' => '',
                        ),
                    )) ?>
                </div>
            </div>
        </div>
    </div>
</div>