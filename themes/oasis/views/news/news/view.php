<?php
/**
 * Отображение для ./themes/default/views/news/news/news.php:
 *
 * @category YupeView
 * @package  YupeCMS
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 *
 * @var $this NewsController
 * @var $model News
 **/
?>
<?php
$this->title = $model->title;
$this->description = $model->description;
$this->keywords = $model->keywords;
?>

<?php
$this->breadcrumbs = [
    Yii::t('NewsModule.news', 'News') => ['/news/news/index'],
    $model->title
];
?>

<div class="info-wrap">
    <div class="container">
        <div class="info">
            <div class="head info-head">
                <div class="main-news-item">
                    <img src="/web/images/news.png" alt="">
                    <div class="main-news-content">
                        <span><?= Yii::app()->getDateFormatter()->formatDateTime(
                                $model->date,
                                "long",
                                false
                            ); ?></span>
                    </div>
                </div>

                <p><?= CHtml::encode($model->title); ?></p>
            </div>
            <div class="info-items content">
                <p> <?= $model->full_text; ?></p>
            </div>
        </div>
    </div>
</div>
<?php $this->widget('application.modules.news.widgets.LastNewsWidget', ['view' => 'view']); ?>
