<?php Yii::import('application.modules.news.NewsModule'); ?>

<?php if (isset($models) && $models != []): ?>
    <div class="main-news-wrap">
        <div class="container">
            <div class="main-news">
                <div class="head">
                    <p>Другие новости </p>
                </div>
                <div class="main-news-items">

                    <?php foreach ($models as $model):
                        $url = Yii::app()->createUrl('/news/news/view', ['slug' => $model->slug]);
                        ?>
                        <div class="main-news-item">
                            <img src="/web/images/news.png" alt="">
                            <div class="main-news-content">
                                <span><?= Yii::app()->getDateFormatter()->formatDateTime(
                                        $model->date,
                                        "long",
                                        false
                                    ); ?></span>
                                <a href="<?= $url ?>" class="main-news__head" title="<?= $model->title ?>"><?= $model->title ?></a>
                                <p><?= $model->getQuote(150) ?><a href="<?= $url ?>" title="<?= $model->title ?>">Читать дальше</a></p>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
                <div class="link main-news-link">
                    <a href="/news" title="Перейти к новостям">Перейти к новостям</a>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>