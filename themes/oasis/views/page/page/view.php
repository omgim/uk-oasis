<?php
/* @var $model Page */
/* @var $this PageController */

if ($model->layout) {
    $this->layout = "//layouts/{$model->layout}";
}

$this->title = $model->title;
$this->breadcrumbs = $this->getBreadCrumbs();
$this->description = $model->description ?: Yii::app()->getModule('yupe')->siteDescription;
$this->keywords = $model->keywords ?: Yii::app()->getModule('yupe')->siteKeyWords;
?>

<div class="info-wrap">
    <div class="container">
        <div class="info">
            <div class="head info-head">
                <p><?= CHtml::encode($model->title); ?></p>
            </div>
            <div class="info-items content">
                <p>    <?= $model->body; ?></p>
            </div>
        </div>
    </div>
</div>
