<?php if (isset($pages) && $pages != []): ?>
    <?php foreach ($pages as $model): ?>
        <div class="info-item">
            <div class="info-item-head">
                <div class="info-item-head-icons">
                    <div class="info-item-head__icon"></div>
                </div>
                <div class="info-item-head__text"><?= $model->title ?></div>
            </div>
            <div class="info-item-content">
                <?= $model->body ?>
            </div>
        </div>
    <?php endforeach; ?>

<?php endif; ?>
